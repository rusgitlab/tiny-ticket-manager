version: '3'

networks:
  frontend:
    driver: ${NETWORKS_DRIVER}
  backend:
    driver: ${NETWORKS_DRIVER}

volumes:
  mysql:
    driver: ${VOLUMES_DRIVER}
services:

### Workspace Utilities ##################################
    workspace:
      build:
        context: ./workspace
        args:
          - PHP_VERSION=${WORKSPACE_PHP_VERSION}
          - INSTALL_XDEBUG=${WORKSPACE_INSTALL_XDEBUG}
          - INSTALL_SSH2=${WORKSPACE_INSTALL_SSH2}
          - INSTALL_NODE=${WORKSPACE_INSTALL_NODE}
          - NODE_VERSION=${WORKSPACE_NODE_VERSION}
          - COMPOSER_GLOBAL_INSTALL=${WORKSPACE_COMPOSER_GLOBAL_INSTALL}
          - COMPOSER_REPO_PACKAGIST=${WORKSPACE_COMPOSER_REPO_PACKAGIST}
          - INSTALL_WORKSPACE_SSH=${WORKSPACE_INSTALL_WORKSPACE_SSH}
          - PUID=${APP_PUID}
          - PGID=${APP_PGID}
          - TZ=${WORKSPACE_TIMEZONE}
      volumes:
        - ${APP_CODE_PATH_HOST}:${APP_CODE_PATH_CONTAINER}
        - ${WEB_CODE_PATH_HOST}:${WEB_CODE_PATH_CONTAINER}
      extra_hosts:
        - "dockerhost:${DOCKER_HOST_IP}"
      ports:
        - "${WORKSPACE_SSH_PORT}:22"
      tty: true
      environment:
        - PHP_IDE_CONFIG=${PHP_IDE_CONFIG}
      networks:
        - frontend
        - backend

### PHP-FPM ##############################################

    php-fpm:
      build:
        context: ./php-fpm
        args:
          - PHP_VERSION=${PHP_FPM_PHP_VERSION}
          - INSTALL_XDEBUG=${PHP_FPM_INSTALL_XDEBUG}
          - INSTALL_SSH2=${PHP_FPM_INSTALL_SSH2}
          - INSTALL_OPCACHE=${PHP_FPM_INSTALL_OPCACHE}
          - INSTALL_MYSQLI=${PHP_FPM_INSTALL_MYSQLI}
          - INSTALL_INTL=${PHP_FPM_INSTALL_INTL}
          - INSTALL_BCMATH=${PHP_FPM_INSTALL_BCMATH}
          - PUID=${APP_PUID}
      volumes:
        - ./php-fpm/php${PHP_FPM_PHP_VERSION}.ini:/usr/local/etc/php/php.ini
        - ${APP_CODE_PATH_HOST}:${APP_CODE_PATH_CONTAINER}
        - ${WEB_CODE_PATH_HOST}:${WEB_CODE_PATH_CONTAINER}
      expose:
        - "9000"
      extra_hosts:
        - "dockerhost:${DOCKER_HOST_IP}"
      environment:
        - PHP_IDE_CONFIG=${PHP_IDE_CONFIG}
        - FAKETIME=${PHP_FPM_FAKETIME}
      depends_on:
        - workspace
      networks:
        - backend

### NGINX Server #########################################

    nginx:
      hostname: ${API_DOMAIN}
      container_name: ${API_DOMAIN}
      build:
        context: ./nginx
        args:
          - PHP_UPSTREAM_CONTAINER=${NGINX_PHP_UPSTREAM_CONTAINER}
          - PHP_UPSTREAM_PORT=${NGINX_PHP_UPSTREAM_PORT}
          - PUID=${APP_PUID}
          - PGID=${APP_PUID}
      volumes:
        - ${APP_CODE_PATH_HOST}:${APP_CODE_PATH_CONTAINER}
        - ${WEB_CODE_PATH_HOST}:${WEB_CODE_PATH_CONTAINER}
        - ${NGINX_HOST_LOG_PATH}:/var/log/nginx
        - ${NGINX_SITES_PATH}:/etc/nginx/sites-available
      ports:
        - "${NGINX_HOST_HTTP_PORT}:80"
        - "${NGINX_HOST_HTTPS_PORT}:443"
      depends_on:
        - php-fpm
      networks:
        - frontend
        - backend

### MySQL ################################################

    mysql:
      build:
        context: ./mysql
        args:
          - MYSQL_VERSION=${MYSQL_VERSION}
      environment:
        - MYSQL_DATABASE=${MYSQL_DATABASE}
        - MYSQL_USER=${MYSQL_USER}
        - MYSQL_PASSWORD=${MYSQL_PASSWORD}
        - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
        - TZ=${WORKSPACE_TIMEZONE}
      volumes:
        - ${DATA_PATH_HOST}/mysql:/var/lib/mysql
        - ${MYSQL_ENTRYPOINT_INITDB}:/docker-entrypoint-initdb.d
      ports:
        - "${MYSQL_PORT}:3306"
      networks:
        - backend
