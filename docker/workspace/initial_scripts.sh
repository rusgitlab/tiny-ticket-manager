#!/usr/bin/env bash

su -c "composer install && composer dump-autoload" -s /bin/sh workspace &&
su -c "php -r \"file_exists('.env') || copy('.env.example', '.env');\"" -s /bin/sh workspace &&
su -c "
    php artisan storage:link &&
    php artisan key:generate --force &&
    php artisan jwt:secret -f &&
    php artisan config:clear &&
    php artisan view:clear &&
    php artisan migrate --force &&
    php artisan db:seed --force &&
    php artisan queue:restart &&
    php artisan up" -s /bin/sh workspace
