###########################################################
###################### General Setup ######################
###########################################################

### Paths #################################################

# Point to the path of your applications code on your host
APP_CODE_PATH_HOST=../api
WEB_CODE_PATH_HOST=../web

# Point to where the `APP_CODE_PATH_HOST` should be in the container. You may add flags to the path `:cached`, `:delegated`. When using Docker Sync add `:nocopy`
APP_CODE_PATH_CONTAINER=/var/www:cached
WEB_CODE_PATH_CONTAINER=/var/www/web:cached

APP_PUID=1001
APP_PGID=1001

# Choose storage path on your machine. For all storage systems
DATA_PATH_HOST=~/.docker/tiny-ticket-manager

### Drivers ################################################

# All volumes driver
VOLUMES_DRIVER=local

# All Networks driver
NETWORKS_DRIVER=bridge

### Docker compose files ##################################

# Select which docker-compose files to include. If using docker-sync append `:docker-compose.sync.yml` at the end
COMPOSE_FILE=docker-compose.yml

# Change the separator from : to ; on Windows
COMPOSE_PATH_SEPARATOR=:

# Define the prefix of container names. This is useful if you have multiple projects that use reports to have seperate containers per project.
COMPOSE_PROJECT_NAME=tiny-ticket-manager

### PHP Interpreter #######################################

# Select the PHP Interpreter. Accepted values: hhvm - php-fpm
PHP_INTERPRETER=php-fpm

### Docker Host IP ########################################

# Enter your Docker Host IP (will be appended to /etc/hosts). Default is `10.0.75.1`
DOCKER_HOST_IP=10.0.75.1

### Remote Interpreter ####################################

# Choose a Remote Interpreter entry matching name. Default is `tiny-ticket-manager`
PHP_IDE_CONFIG=serverName=tiny-ticket-manager

### Windows Path ##########################################

# A fix for Windows users, to ensure the application path works
COMPOSE_CONVERT_WINDOWS_PATHS=1

### Environment ###########################################

# If you need to change the sources (i.e. to China), set CHANGE_SOURCE to true
CHANGE_SOURCE=false

### Docker Sync ###########################################

# If you are using Docker Sync. For `osx` use 'native_osx', for `windows` use 'unison', for `linux` docker-sync is not required
DOCKER_SYNC_STRATEGY=native_osx

API_DOMAIN=

###########################################################
################ Containers Customization #################
###########################################################

### WORKSPACE #############################################

WORKSPACE_INSTALL_NODE=true
WORKSPACE_NODE_VERSION=node
WORKSPACE_PHP_VERSION=7.2
WORKSPACE_COMPOSER_GLOBAL_INSTALL=true
WORKSPACE_COMPOSER_REPO_PACKAGIST=
WORKSPACE_INSTALL_WORKSPACE_SSH=true
WORKSPACE_INSTALL_XDEBUG=true
WORKSPACE_INSTALL_SSH2=true
WORKSPACE_INSTALL_AMQP=true
WORKSPACE_INSTALL_LARAVEL_INSTALLER=true
WORKSPACE_INSTALL_MC=true
WORKSPACE_TIMEZONE=UTC
WORKSPACE_SSH_PORT=2223

### PHP_FPM ###############################################

PHP_FPM_PHP_VERSION=7.2
PHP_FPM_INSTALL_MYSQLI=true
PHP_FPM_INSTALL_INTL=true
PHP_FPM_INSTALL_OPCACHE=true
PHP_FPM_INSTALL_PHPREDIS=true
PHP_FPM_INSTALL_XDEBUG=true
PHP_FPM_INSTALL_AMQP=true
PHP_FPM_INSTALL_SSH2=true
PHP_FPM_FAKETIME=-0
PHP_FPM_INSTALL_BCMATH=true

### PHP_WORKER ############################################

PHP_WORKER_PHP_VERSION=7.2
PHP_WORKER_INSTALL_PGSQL=false
PHP_WORKER_INSTALL_BCMATH=false
PHP_WORKER_INSTALL_SOAP=false
PHP_WORKER_INSTALL_ZIP_ARCHIVE=false

### NGINX #################################################

NGINX_HOST_HTTP_PORT=80
NGINX_HOST_HTTPS_PORT=443
NGINX_HOST_LOG_PATH=./logs/nginx/
NGINX_SITES_PATH=./nginx/sites/
NGINX_PHP_UPSTREAM_CONTAINER=php-fpm
NGINX_PHP_UPSTREAM_PORT=9000

### MYSQL #################################################

MYSQL_VERSION=5.7
MYSQL_DATABASE=tiny_ticket_db
MYSQL_USER=tiny_db_user
MYSQL_PASSWORD=secret
MYSQL_PORT=3306
MYSQL_ROOT_PASSWORD=root
MYSQL_ENTRYPOINT_INITDB=./mysql/docker-entrypoint-initdb.d
