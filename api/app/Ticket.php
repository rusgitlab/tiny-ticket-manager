<?php

namespace App;

use Brexis\LaravelWorkflow\Traits\WorkflowTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Ticket
 * @package App
 */
class Ticket extends Model
{
    use WorkflowTrait;

    protected $attributes = [
        'status' => 'created',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'attachmentFile'
    ];

    /**
     * @return string
     */
    public function getAttachmentDirectory(): string
    {
        return 'tickets/' . $this->id;
    }

    /**
     * @return string
     */
    public function getAttachmentFullPath(): string
    {
        return $this->getAttachmentDirectory() . '/' . $this->attachmentFile;
    }

    /**
     * @return array
     */
    public function getAbilities(): array
    {
        $canAccept = auth()->user()->can('accept', $this);
        $canReject = auth()->user()->can('reject', $this);

        return [
            'canView' => auth()->user()->can('view', new self()),
            'canCreate' => auth()->user()->can('create', new self()),
            'canDelete' => auth()->user()->can('delete', $this),
            'canApplyWorkflow' => $canAccept || $canReject,
            'canAccept' => $canAccept,
            'canReject' => $canReject
        ];
    }
}
