<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const ROLE_OPERATOR = 1;
    const ROLE_MANAGER = 2;
    const ROLE_ADMINISTRATOR = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return bool
     */
    public function isOperator(): bool
    {
        return $this->role_id == self::ROLE_OPERATOR;
    }

    /**
     * @return bool
     */
    public function isManager(): bool
    {
        return $this->role_id == self::ROLE_MANAGER;
    }

    /**
     * @return bool
     */
    public function isAdministrator(): bool
    {
        return $this->role_id == self::ROLE_ADMINISTRATOR;
    }
}
