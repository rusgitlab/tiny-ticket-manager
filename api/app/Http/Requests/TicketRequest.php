<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class TicketRequest
 * @package App\Http\Requests
 */
class TicketRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'min:3', 'max: 256'],
            'pdfFile' => ['mimes:pdf', 'max:10240'],
        ];
    }
}
