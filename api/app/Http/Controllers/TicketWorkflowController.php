<?php

namespace App\Http\Controllers;

use App\Ticket;

/**
 * Class TicketWorkflowController
 * @package App\Http\Controllers
 */
class TicketWorkflowController extends Controller
{
    /**
     * @param Ticket $ticket
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function accept(Ticket $ticket)
    {
        $this->authorize('accept', $ticket);

        $ticket->workflow_apply('accept');
        $ticket->save();
        return redirect()->back()->withStatus(__('Ticket successfully accepted.'));
    }

    /**
     * @param Ticket $ticket
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function reject(Ticket $ticket)
    {
        $this->authorize('reject', $ticket);

        $ticket->workflow_apply('reject');
        $ticket->save();
        return redirect()->back()->withStatus(__('Ticket successfully rejected.'));
    }
}
