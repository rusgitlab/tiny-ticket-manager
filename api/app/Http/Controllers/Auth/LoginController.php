<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;

/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function login()
    {
        $role = request()->role;

        if (isset($role) && !in_array($role, [User::ROLE_OPERATOR, User::ROLE_ADMINISTRATOR, User::ROLE_MANAGER])) {
            throw new \Exception('Undefined role');
        }

        if (!$role) {
            $user = User::firstOrFail();
        } else {
            $user = User::where('role_id', $role)->firstOrFail();
        }

        auth()->login($user);

        return redirect()->back();
    }
}
