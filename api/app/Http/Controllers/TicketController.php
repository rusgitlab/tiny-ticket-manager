<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\Http\Requests\TicketRequest;
use Illuminate\Support\Facades\Storage;

/**
 * Class TicketController
 * @package App\Http\Controllers
 */
class TicketController extends Controller
{
    /**
     * @param Ticket $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Ticket $model)
    {
        return view('tickets.index', ['tickets' => $model->paginate(15)]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', new Ticket());

        return view('tickets.create');
    }

    /**
     * @param TicketRequest $request
     * @param Ticket $model
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(TicketRequest $request, Ticket $model)
    {
        $this->authorize('create', new Ticket());

        $model = $model->create($request->all());

        $file = $request->file('pdfFile');
        if ($file) {
            Storage::putFileAs($model->getAttachmentDirectory(), $file, $file->getClientOriginalName());
            $model->update(['attachmentFile' => $file->getClientOriginalName()]);
        }

        return redirect()->route('ticket.index')->withStatus(__('Ticket successfully created.'));
    }

    /**
     * @param Ticket $ticket
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Ticket $ticket)
    {
        $this->authorize('view', $ticket);

        return view('tickets.show', compact('ticket'));
    }

    /**
     * @param Ticket $ticket
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Ticket $ticket)
    {
        $this->authorize('delete', $ticket);

        Storage::delete($ticket->getAttachmentFullPath());
        Storage::deleteDirectory($ticket->getAttachmentDirectory());

        $ticket->delete();

        return redirect()->route('ticket.index')->withStatus(__('Ticket successfully deleted.'));
    }

    /**
     * @param Ticket $ticket
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function downloadAttachment(Ticket $ticket)
    {
        $this->authorize('downloadAttachment', $ticket);

        abort_unless($ticket->exists, 404);

        return response()->download(
            storage_path('app/' . $ticket->getAttachmentFullPath()),
            $ticket->attachmentFile,
            [
                'Content-Type' => Storage::getMimeType($ticket->getAttachmentFullPath()),
                'Content-Disposition' => "attachment; filename=\"{$ticket->attachmentFile}\"",
                'Content-Length' => Storage::getSize($ticket->getAttachmentFullPath())
            ]);
    }
}
