<?php

namespace App\Policies;

use App\Ticket;
use App\User;
use Workflow;

/**
 * Class TicketPolicy
 * @package App\Policies
 */
class TicketPolicy
{
    /**
     * @param User $user
     * @param Ticket $ticket
     * @return bool
     */
    public function view(User $user, Ticket $ticket): bool
    {
        return true;
    }

    /**
     * @param User $user
     * @param Ticket $ticket
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->isOperator();
    }

    /**
     * @param User $user
     * @param Ticket $ticket
     * @return bool
     */
    public function delete(User $user, Ticket $ticket): bool
    {
        return $user->isAdministrator();
    }

    /**
     * @param User $user
     * @param Ticket $ticket
     * @return bool
     */
    public function accept(User $user, Ticket $ticket): bool
    {
        $workflow = Workflow::get($ticket);
        return $user->isManager() && $workflow->can($ticket, 'accept');
    }

    /**
     * @param User $user
     * @param Ticket $ticket
     * @return bool
     */
    public function reject(User $user, Ticket $ticket): bool
    {
        $workflow = Workflow::get($ticket);
        return $user->isManager() && $workflow->can($ticket, 'reject');
    }

    /**
     * @param User $user
     * @param Ticket $ticket
     * @return bool
     */
    public function downloadAttachment(User $user, Ticket $ticket): bool
    {
        return $user->isManager();
    }
}
