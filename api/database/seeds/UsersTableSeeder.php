<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        User::firstOrCreate(
            ['email' => 'operator@mail.com', 'name' => 'Operator'],
            ['password' => Hash::make('secret'), 'role_id' => 1]
        );

        User::firstOrCreate(
            ['email' => 'manager@mail.com', 'name' => 'Manager'],
            ['password' => Hash::make('secret'), 'role_id' => 2]
        );

        User::firstOrCreate(
            ['email' => 'administrator@mail.com', 'name' => 'Administrator'],
            ['password' => Hash::make('secret'), 'role_id' => 3]
        );

    }
}
