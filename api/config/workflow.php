<?php

return [
    'straight'   => [
        'type'          => 'state_machine',
        'marking_store' => [
            'type' => 'single_state',
            'arguments' => ['status']
        ],
        'supports'      => ['App\Ticket'],
        'places'        => ['created', 'accepted', 'rejected'],
        'transitions'   => [
            'accept' => [
                'from' => 'created',
                'to'   => 'accepted',
            ],
            'reject' => [
                'from' => 'created',
                'to'   => 'rejected',
            ]
        ],
    ]
];
