@extends('layouts.app', ['title' => __('Ticket Management')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Tickets') }}</h3>
                            </div>
                            @if(auth()->user()->can('create', new \App\Ticket()))
                            <div class="col-4 text-right">
                                <a href="{{ route('ticket.create') }}" class="btn btn-sm btn-primary">{{ __('Add ticket') }}</a>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('Title') }}</th>
                                    <th scope="col">{{ __('PDF specification') }}</th>
                                    <th scope="col">{{ __('Creation Date') }}</th>
                                    <th scope="col">{{ __('Workflow') }}</th>
                                    <th scope="col">{{ __('Manage Workflow') }}</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tickets as $ticket)
                                    <tr>
                                        <td>{{ $ticket->title }}</td>
                                        <td>
                                            @if($ticket->attachmentFile)
                                                @if(auth()->user()->can('downloadAttachment', $ticket))
                                                    <a href="{{ route('ticket.attachment.get', $ticket) }}">
                                                        {{ $ticket->attachmentFile }}
                                                    </a>
                                                @else
                                                    {{ $ticket->attachmentFile }}
                                                @endif
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>{{ $ticket->created_at->format('d/m/Y H:i') }}</td>
                                        <td>{{ strtoupper($ticket->status) }}</td>
                                        <td>
                                        @if($ticket->getAbilities()['canApplyWorkflow'])
                                            <div class="row">
                                            <form class="col-md-3" method="post" action="{{ route('ticket.accept', ['ticket' => $ticket]) }}">
                                                {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-success btn-sm mt-4">{{ __('Accept') }}</button>
                                            </form>
                                            <form class="col-md-3" method="post" action="{{ route('ticket.reject', ['ticket' => $ticket]) }}">
                                                {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm mt-4">{{ __('Reject') }}</button>
                                            </form>
                                            </div>
                                        @endif
                                        </td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <form action="{{ route('ticket.destroy', $ticket) }}" method="post">
                                                        {{ csrf_field() }}

                                                        <a class="dropdown-item" href="{{ route('ticket.show', $ticket) }}">{{ __('Details') }}</a>

                                                        @if($ticket->getAbilities()['canDelete'])
                                                            <button type="button" class="dropdown-item" onclick="confirm('{{ __("Are you sure you want to delete this ticket?") }}') ? this.parentElement.submit() : ''">
                                                                {{ __('Delete') }}
                                                            </button>
                                                        @endif
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{ $tickets->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection